object ConfigurationData {
    const val applicationId = "com.joao.learnengo"
    const val compileSdk = 33
    const val buildToolsVersion = "30.0.3"
    const val minSdk = 24
    const val targetSdk= 33
    const val versionCode = 1
    const val versionName = "1.0"
}