object Versions {
    const val ktlint = "0.45.2"
    const val activity-compose = "1.5.1"
    const val lifecycle-runtime = "2.3.1"
    const val androidx-core = "1.10.1"
    const val kotlin-compile-extension = "1.3.2"
}

object Libs {
    const val androidGradlePlugin = "com.android.tools.build:gradle:7.2.1"
    const val kotlinCompileExtension = "com.android.tools.build:gradle:7.2.1"
    //...

    object Kotlin {
        private const val version = "1.7.0"
        const val stdlib = "org.jetbrains.kotlin:kotlin-stdlib-jdk8:$version"
        //...
    }

    object AndroidX {
        const val appcompatCompose = "androidx.activity:activity-compose:${Versions.activity-compose}"
        const val lifecycleRuntime = "androidx.lifecycle:lifecycle-runtime-ktx:${Versions.lifecycle-runtime}"
        const val androidxCore = "androidx.core:core-ktx:${Versions.androidx-core}"
        const val

        object Compose {
            const val composeUi = "androidx.compose.ui:ui"
            const val composeuiGraphics = "androidx.compose.ui:ui-graphics"
            const val composeUiToolingPreview = "androidx.compose.ui:ui-tooling-preview"
            const val composeUiMaterial = "androidx.compose.material3:material3"
        }

        object Test {
            const val junit = "androidx.test.ext:junit:1.1.3"
            const val espresso = "androidx.test.espresso:espresso-core:3.4.0"
            const val composeJunit = "androidx.compose.ui:ui-test-junit4")
        }

        object Debug {
            const val composeTooling = "androidx.compose.ui:ui-tooling"
            const val composeUiManifest = "androidx.compose.ui:ui-test-manifest"
        }

    }
    object Test {
        const val junit = "junit:junit:4.13.2")

    }
    }

object Projects {
    object Modules {
        const val coreDs = ":core-ds"
    }

    object Bom {
        const val kotlin = "org.jetbrains.kotlin:kotlin-bom:1.8.0"
        const val compose = "androidx.compose:compose-bom:2022.10.00"
        const val composeTest = "androidx.compose:compose-bom:2022.10.00"
    }
}
